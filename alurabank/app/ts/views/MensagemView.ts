import { View } from "./View";

export class MensagemView extends View<string> {
        
        template(model: string): string { // Apresento a mensagem retornada pelo método template() e coloco no seletor do elemento do DOM que defini no construtor
            return `<p class="alert alert-info">${model}</p>`;
        }

}
    

