import { View } from "./View";
import { Negociacoes } from "../models/Negociacoes"

export class NegociacoesView extends View<Negociacoes> {

        template(model: Negociacoes): string { // Apresento a minha lista de negociações retornada pelo método template() e coloco no seletor do elemento do DOM que defini no construtor
            return `
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>DATA</th>
                        <th>QUANTIDADE</th>
                        <th>VALOR</th>
                        <th>VOLUME</th>
                    </tr>
                </thead>

                <tbody>
                    ${model.pegaArray().map(negociacao => {
                        return `
                        <tr>
                            <td>${negociacao.data.getDate()}/${negociacao.data.getMonth() + 1}/${negociacao.data.getFullYear()}</td>
                            <td>${negociacao.quantidade}</td>
                            <td>${negociacao.valor}</td>
                            <td>${negociacao.volume}</td>                    
                        </tr>
                        
                        `
                    }).join("")} 
                </tbody>

                <tfoot>
                </tfoot>
            </table>       
            `;
        } // join() concatena todos os itens do Array para uma String grande
        
}

