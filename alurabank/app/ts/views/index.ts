export * from "./View";
export * from "./MensagemView";
export * from "./NegociacoesView";

/* Posso simplificar a importação dos módulos através da estratégia Barrel (barril). 
Nela, um módulo importa e exporta todos os artefatos de uma pasta permitindo assim que apenas
o barril seja importado na cláusula from. */