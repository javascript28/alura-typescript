import { logarTempoDeExecucao } from "../helpers/decorators/logarTempoDeExecucao";

export abstract class View<T> { // Indico que essa classe é do tipo genérica e especifico o tipo nas classes filhas
    
    private elemento: JQuery;
    private escapar: boolean;
    
    constructor(seletor: string, escapar: boolean = false) { // ? faz com que o parâmetro se torne opcional, devem ser sempre os últimos
        this.elemento = $(seletor); // retorna o primeiro elemento filho de um determinado seletor CSS 
        this.escapar = escapar;
    }

    //@logarTempoDeExecucao(true)
    update(model: T): void {
        let template = this.template(model);

        if(this.escapar) 
            template = template.replace(/<script>[\s\S]*?<\/script>/, ''); // Expressão regular
        this.elemento.html(template); // Pego o resultado do template e atribuo na propriedade innerHTML que converte para elementos do DOM
    }

    abstract template(model: T): string;  // Apresento a mensagem retornada pelo método template() e coloco no seletor do elemento do DOM que defini no construtor    
}

