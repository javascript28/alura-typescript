import { Negociacao } from "./Negociacao"
import { MeuObjeto } from "./index"

export class Negociacoes implements MeuObjeto<Negociacoes> { // Uso export para poder importar nas outras classes

    private negociacoes: Negociacao[] = [];
    //private negociacoes: Array<Negociacao> = []; outra forma de declarar um Array

    adiciona(negociacao: Negociacao): void {
        this.negociacoes.push(negociacao);
    }

    pegaArray(): Negociacao[] {
        return ([] as Negociacao[]).concat(this.negociacoes); // crio uma cópia do array com o concat
    }

    paraTexto(): void {
        console.log(JSON.stringify(this.negociacoes));
    }

    ehIgual(negociacoes: Negociacoes): boolean {
        return JSON.stringify(this.negociacoes) == JSON.stringify(negociacoes.pegaArray);
    }
}