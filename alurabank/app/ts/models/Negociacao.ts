import { MeuObjeto } from "../models/index";

export class Negociacao implements MeuObjeto<Negociacao> { // Uso export para poder importar nas outras classes

    // O underline significa que uma Negociacao depois de criada não pode ter suas propriedades alteradas sem acessar os métodos da própria classe
    // readonly significa que as propriedades não podem receber uma nova atribuição
    constructor(readonly data: Date, readonly quantidade: number, readonly valor: number){
    }

    get volume(){
        return this.quantidade * this.valor;
    }

    paraTexto(): void {
        console.log(
        `Data: ${this.data}
        Quantidade: ${this.quantidade}
        Valor: ${this.valor}
        Volume: ${this.volume}`);
    }

    ehIgual(negociacao: Negociacao): boolean {
        return this.data.getDate() == negociacao.data.getDate()
        && this.data.getMonth() == negociacao.data.getMonth()
        && this.data.getFullYear() == negociacao.data.getFullYear();
    }

    


}