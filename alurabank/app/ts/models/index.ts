export * from "./Imprimivel";
export * from "./Negociacao";
export * from "./Negociacoes";
export * from "./NegociacaoParcial";
export * from "./Igualavel";
export * from "./MeuObjeto";

/* Posso simplificar a importação dos módulos através da estratégia Barrel (barril). 
Nela, um módulo importa e exporta todos os artefatos de uma pasta permitindo assim que apenas
o barril seja importado na cláusula from. */