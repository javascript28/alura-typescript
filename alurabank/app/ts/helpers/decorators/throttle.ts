export function throttle(milissegundos = 500) {
    /* Do jeito que a aplicação esta, se clicar muitas vezes em seguida no botão de importar, é executado várias requisições para o servidor. 
    Para se prevenir disso, posso ignorar cliques que estejam dentro de uma janela de tempo definida. Aqui no caso, se o usuário clicar em 
    menos de meio segundo, cancelo a ação dele e inicio uma nova. A solução utiliza um timer para processar as ações do usuário.
    */

    return function(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const metodoOriginal = descriptor.value;
        let timer = 0;
        
        descriptor.value = function(...args: any[]) {
            if(event){
                event.preventDefault(); // Por padrão, ao chamar o evento do formulário é recarregado a página. O preventDefault() não deixa isso acontecer
            }
            
            clearInterval(timer);
            timer = setTimeout(() => metodoOriginal.apply(this, args), milissegundos);
        }

        return descriptor;
    }
}