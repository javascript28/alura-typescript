import { Imprimivel } from "../models/Imprimivel";

export function imprime(...objetos: Imprimivel[]) { // Recebo como parâmetro todo mundo que é Imprimivel (Polimorfismo)
    objetos.forEach(objeto => objeto.paraTexto()); // Para cada negociacao vou chamar o paraTexto()
}