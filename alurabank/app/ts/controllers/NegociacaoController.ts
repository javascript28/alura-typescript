import { NegociacoesView, MensagemView } from "../views/index"; // Estratégia Barrel
import { Negociacoes, Negociacao } from "../models/index"; // Estratégia Barrel
import { logarTempoDeExecucao } from "../helpers/decorators/index";
import { domInject, throttle } from "../helpers/decorators/index";
import { NegociacaoParcial } from  "../models/NegociacaoParcial";
import { NegociacaoService } from "../services/index";
import { imprime } from "../helpers/index";


export class NegociacaoController { // Uso export para poder importar nas outras classes

    @domInject("#data")
    private inputData: JQuery;

    @domInject("#quantidade")
    private inputQuantidade: JQuery;

    @domInject("#valor")
    private inputValor: JQuery;
    private negociacoes: Negociacoes = new Negociacoes();
    private negociacoesView = new NegociacoesView("#negociacoesView");
    private mensagemView = new MensagemView("#mensagemView");
    private negociacaoService = new NegociacaoService();

    constructor(){
        this.negociacoesView.update(this.negociacoes);
    }

    @logarTempoDeExecucao()
    @throttle()
    adiciona(){
        let data = new Date(this.inputData.val().replace(/-/g, ","));

        if(!this.ehFinalDeSemana) {
            this.mensagemView.update("Somente negociações em dias úteis, por favor!");
            return;
        }

        const negociacao = new Negociacao(new Date(this.inputData.val().replace(/-/g, ',')), parseInt(this.inputQuantidade.val()), parseFloat(this.inputValor.val())); // Seto os valores que digitei no input
        
        
        this.negociacoes.adiciona(negociacao);
        imprime(negociacao, this.negociacoes);
        
        this.negociacoesView.update(this.negociacoes);
        this.mensagemView.update("Negociação adicionada com sucesso!");
    }

    private ehFinalDeSemana(data: Date) {
        return data.getDay() == DiaDaSemana.Domingo  || data.getDay() == DiaDaSemana.Sabado;
    }

    @throttle(500)
    async importarDados() {

        try{
            const negociacoesParaImportar = await this.negociacaoService.obterNegociacoes(resposta => {
            
            if(resposta.ok) {
                return resposta;
            } else {
                throw new Error(resposta.statusText);
            }
        });

        // Só será executado quando minha PROMISE for cumprida, usando o Asynx/Wait ele me dá essa forma diferente de esperar a resposta
        // Parece sincrona mas está sendo assincrona
        const negociacoesImportadas = this.negociacoes.pegaArray();

        negociacoesParaImportar.filter(negociacao => !negociacoesImportadas.some(importada => 
            negociacao.ehIgual(importada)))
            .forEach(negociacao => this.negociacoes.adiciona(negociacao));
            
        this.negociacoesView.update(this.negociacoes);

        } catch(erro) {
            this.mensagemView.update(erro.message);
        }
    }

}

// Começa do 0, mantive a ordem pois em JS essa é a ordem para validar os dias da semana
// Posso atribuir os valores se quiser
enum DiaDaSemana {

    Domingo,
    Segunda,
    Terca,
    Quarta,
    Quinta,
    Sexta,
    Sabado
}