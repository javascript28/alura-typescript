import { Negociacao, NegociacaoParcial } from "../models/index";

export class NegociacaoService {

    obterNegociacoes(handler: HandlerFunction): Promise<Negociacao[]> { // Em vez de usar function que é genérico eu uso HandlerFunction que é um tipo mais específico e defino na interface HandlerFunction o que precisa receber(Response) e devolver(Response)   
        return fetch('http://localhost:8080/dados')
        .then(resposta => handler(resposta)) 
        .then(resposta => resposta.json()) // converto os dados para Json
        .then((dados: NegociacaoParcial[]) =>  
            dados
            .map(dado => new Negociacao(new Date(), dado.vezes, dado.montante)) // converto a lista de objetos que tem montante e vezes para uma lista do tipo negociacoes e adiciono na lista do controller
            // .forEach(negociacao => this.negociacoes.adiciona(negociacao)); // Digo que para cada item do array de Negociacao eu adiciono em Negociacoes
            // this.negociacoesView.update(this.negociacoes);
        )
        .catch(erro => {
            console.log(erro);
            throw new Error("Não foi possível importar as negociações!");       
        });
    }
}

export interface HandlerFunction { // Crio uma interface para definir as funções que eu passo como parâmetro  
    (resposta: Response): Response
}