# Formação Angular | Alura - Cursos online de tecnologia

**1.** Na pasta **alurabank** execute os comandos:
 * npm install typescript@2.3.2 --save-dev; 
 * npm install @types/jquery@2.0.42 --save-dev;
 * npm install lite-server@2.3.0 --save-dev;
 * npm run server.

**2.** Na pasta **api** execute o comando:
 * npm start.

**3.** Acesse o endereço [http://localhost:3000/](http://localhost:3000/) 
